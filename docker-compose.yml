version: '3'
services:
  mosquitto:
    image: eclipse-mosquitto
    container_name: mqtt-broker
    deploy:
      replicas: 1
    restart: unless-stopped
    ports:
      - '1883:1883'
      - '127.0.0.1:9001:9001'
    volumes:
      - /iot/mosquitto/:/mosquitto/
  influxdb:
    image: influxdb:1.8.10
    container_name: database
    deploy:
      replicas: 1
    restart: unless-stopped
    ports:
      - '127.0.0.1:8086:8086'
    volumes:
      - /iot/influxdb/:/var/lib/influxdb/
    environment:
      - DOCKER_INFLUXDB_INIT_USERNAME=my-user
      - DOCKER_INFLUXDB_INIT_PASSWORD=my-iot-password
      - DOCKER_INFLUXDB_INIT_ORG=iot-org
      - DOCKER_INFLUXDB_INIT_BUCKET=iot-bucket
  telegraf:
    image: telegraf:1.21.2
    container_name: mqtt-subscriber
    deploy:
      replicas: 1
    restart: unless-stopped
    depends_on:
      - mosquitto
      - influxdb
    volumes:
      - /iot/telegraf/:/etc/telegraf/
  grafana:
    image: grafana/grafana:8.3.3
    container_name: dashboard
    restart: unless-stopped
    labels:
      - "traefik.enable=true"
      # SSL redirect requires a separate router (https://github.com/containous/traefik/issues/4688#issuecomment-477800500)
      - "traefik.http.routers.grafana.entryPoints=port80"
      - "traefik.http.routers.grafana.rule=host(`rpi.herrenberger-80.de`)"
      - "traefik.http.middlewares.grafana-redirect.redirectScheme.scheme=https"
      - "traefik.http.middlewares.grafana-redirect.redirectScheme.permanent=true"
      - "traefik.http.routers.grafana.middlewares=grafana-redirect"
      # SSL endpoint
      - "traefik.http.routers.grafana-tls.entryPoints=port443"
      - "traefik.http.routers.grafana-tls.rule=host(`rpi.herrenberger-80.de`)"
      - "traefik.http.routers.grafana-tls.tls=true"
      - "traefik.http.routers.grafana-tls.tls.certResolver=grafana-resolver"
      - "traefik.http.routers.grafana-tls.service=grafana-tls"
      - "traefik.http.services.grafana-tls.loadBalancer.server.port=3000"
    deploy:
      replicas: 1
    restart: unless-stopped
    ports:
      - '3000:3000'
    volumes:
      - /iot/grafana/:/var/lib/grafana/
    environment:
      - GF_SERVER_ROOT_URL=https://rpi.herrenberger-80.de
      - GF_SERVER_DOMAIN=rpi.herrenberger-80.de
      - GF_USERS_ALLOW_SIGN_UP=false
      - GF_SECURITY_ADMIN_USER=admin
      - GF_SECURITY_ADMIN_PASSWORD=
  http2mqtt:
    image: oliverlorenz/http2mqtt:latest
    restart: unless-stopped
    ports:
      - '8080:9001'
    environment:
      - BROKER_URL=mqtt://mqtt-broker
      - TOPIC_BASE=/
    depends_on:
      - mosquitto
  traefik:
    image: "traefik:v2.9"
    container_name: "traefik"
    restart: unless-stopped
    ports:
      - "80:80"
      - "443:443"
      - "8081:8080"
    command:
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entryPoints.port443.address=:443"
      - "--entryPoints.port80.address=:80"
      - "--certificatesResolvers.grafana-resolver.acme.tlsChallenge=true"
      - "--certificatesResolvers.grafana-resolver.acme.email=dominik.basner@gmail.com"
      - "--certificatesResolvers.grafana-resolver.acme.storage=/letsencrypt/acme.json"
    volumes:
      - /iot/traefik-data:/letsencrypt/
      - /var/run/docker.sock:/var/run/docker.sock:ro
  display:
    image: ep-display:latest
    restart: unless-stopped
    privileged: true
    depends_on:
      - mosquitto
    volumes:
      - /dev/:/dev/
