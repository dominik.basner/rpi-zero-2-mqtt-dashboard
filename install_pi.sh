#!/bin/bash
sudo apt-get update
sudo curl -sSL https://get.docker.com | sudo sh
sudo apt-get install libffi-dev libssl-dev
sudo apt install python3-dev
sudo apt-get install -y python3 python3-pip
sudo pip3 install docker-compose
sudo systemctl enable docker
sudo mkdir /iot
sudo mkdir /iot/mosquitto
sudo mkdir /iot/influxdb
sudo mkdir /iot/telegraf
sudo mkdir /iot/grafana
sudo chmod 777 /iot/grafana
sudo chmod 777 /iot/telegraf/telegraf.conf
cd /iot
curl -sL https://gitlab.com/dominik.basner/rpi-zero-2-mqtt-dashboard/-/archive/main/rpi-zero-2-mqtt-dashboard-main.tar.gz | tar zx
mv rpi-zero-2-mqtt-dashboard-main/* .
rm -rf rpi-zero-2-mqtt-dashboard-main
cp mqtt.service /etc/avahi/services/mqtt.service